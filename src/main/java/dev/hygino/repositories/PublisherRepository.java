package dev.hygino.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.hygino.entities.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {

}
