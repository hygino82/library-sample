package dev.hygino.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import dev.hygino.entities.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

    @Query("SELECT obj FROM Book obj WHERE UPPER(obj.author.name) LIKE CONCAT('%', UPPER(:name), '%')")
    Page<Book> findByAuthor(String name, Pageable pageable);

    @Query("SELECT obj FROM Book obj WHERE UPPER(obj.publisher.name) LIKE CONCAT('%', UPPER(:name), '%')")
    Page<Book> findByPublisher(String name, Pageable pageable);
}
