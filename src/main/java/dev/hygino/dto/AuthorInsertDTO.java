package dev.hygino.dto;

import jakarta.validation.constraints.NotBlank;

public class AuthorInsertDTO {
	
	@NotBlank
	private String name;
	
	@NotBlank
	private String country;

	public AuthorInsertDTO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
