package dev.hygino.dto;

import java.time.Instant;

import jakarta.validation.constraints.NotBlank;

public class PublisherDTO {

	private Long id;

	@NotBlank
	private String name;
	private Instant createDate;
	private Instant updateDate;

	public PublisherDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Instant getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Instant createDate) {
		this.createDate = createDate;
	}

	public Instant getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Instant updateDate) {
		this.updateDate = updateDate;
	}
}
