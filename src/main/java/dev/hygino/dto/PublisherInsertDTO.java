package dev.hygino.dto;

import jakarta.validation.constraints.NotBlank;

public class PublisherInsertDTO {

	@NotBlank
	private String name;

	public PublisherInsertDTO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
