package dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.hygino.dto.PublisherDTO;
import dev.hygino.dto.PublisherInsertDTO;
import dev.hygino.services.PublisherService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/publisher")
public class PublisherController {

    private final PublisherService publisherService;

    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @PostMapping
    @Tag(name = "Insere uma nova editora")
    public ResponseEntity<PublisherDTO> insert(@RequestBody @Valid PublisherInsertDTO insertDTO) {
        PublisherDTO dto = publisherService.insert(insertDTO);

        return ResponseEntity.status(201).body(dto);
    }

    @GetMapping("/{id}")
    @Tag(name = "Busca uma editora pelo Id")
    public ResponseEntity<PublisherDTO> findById(@PathVariable("id") Long id) {
        PublisherDTO dto = publisherService.findById(id);
        return ResponseEntity.status(200).body(dto);
    }

    @GetMapping
    @Tag(name = "Busca editoras de forma paginada")
    public ResponseEntity<Page<PublisherDTO>> findAll(Pageable pageable) {
        Page<PublisherDTO> pageDto = publisherService.findAll(pageable);

        return ResponseEntity.status(200).body(pageDto);
    }

    @PutMapping("/{id}")
    @Tag(name = "Atualiza editora por Id")
    public ResponseEntity<PublisherDTO> update(@PathVariable("id") Long id,
            @RequestBody @Valid PublisherInsertDTO insertDTO) {

        PublisherDTO dto = publisherService.update(id, insertDTO);
        return ResponseEntity.status(200).body(dto);
    }

    @DeleteMapping("/{id}")
    @Tag(name = "Remove editora por Id")
    public ResponseEntity<Void> remove(@PathVariable("id") Long id) {
        publisherService.remove(id);
        return ResponseEntity.noContent().build();
    }
}
