package dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.hygino.dto.AuthorDTO;
import dev.hygino.dto.AuthorInsertDTO;
import dev.hygino.services.AuthorService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/author")
public class AuthorController {

	private final AuthorService authorService;

	public AuthorController(AuthorService authorService) {
		this.authorService = authorService;
	}

	@PostMapping
	@Tag(name = "Insere um novo autor")
	public ResponseEntity<AuthorDTO> insert(@RequestBody @Valid AuthorInsertDTO dto) {
		return ResponseEntity.status(201).body(authorService.insert(dto));
	}

	@GetMapping
	@Tag(name = "Busca por autores de forma paginada")
	public ResponseEntity<Page<AuthorDTO>> findAll(Pageable pageable) {
		Page<AuthorDTO> pageDto = authorService.findAll(pageable);
		return ResponseEntity.status(200).body(pageDto);
	}

	@GetMapping("/{id}")
	@Tag(name = "Busca autor por Id")
	public ResponseEntity<AuthorDTO> findById(@PathVariable("id") Long id) {
		AuthorDTO dto = authorService.findById(id);
		return ResponseEntity.status(200).body(dto);
	}

	@PutMapping("/{id}")
	@Tag(name = "Atualiza autor por Id")
	public ResponseEntity<AuthorDTO> update(@PathVariable("id") Long id,
			@RequestBody @Valid AuthorInsertDTO insertDTO) {
		AuthorDTO dto = authorService.update(id, insertDTO);
		return ResponseEntity.status(200).body(dto);
	}
	
	@DeleteMapping("/{id}")
	@Tag(name = "Remove autor por Id")
	public ResponseEntity<Void> remove(@PathVariable("id") Long id){
		authorService.remove(id);
		return ResponseEntity.noContent().build();
	}
}
