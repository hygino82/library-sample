package dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.hygino.dto.BookDTO;
import dev.hygino.dto.BookInsertDTO;
import dev.hygino.services.BookService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;

@RestController
@RequestMapping("api/v1/book")
public class BookController {

	private final BookService bookService;

	public BookController(BookService bookService) {
		this.bookService = bookService;
	}

	@GetMapping
	@Tag(name = "Busca por livros de forma paginada")
	public ResponseEntity<Page<BookDTO>> findAll(Pageable pageable) {
		Page<BookDTO> pageDto = bookService.findAll(pageable);
		return ResponseEntity.status(200).body(pageDto);
	}

	@GetMapping("/{id}")
	@Tag(name = "Busca livro por Id")
	public ResponseEntity<BookDTO> findById(@PathVariable("id") Long id) {
		BookDTO dto = bookService.findById(id);
		return ResponseEntity.status(200).body(dto);
	}

	@PostMapping
	@Tag(name = "Insere um novo livro")
	public ResponseEntity<BookDTO> insert(@RequestBody @Valid BookInsertDTO insertDTO) {
		BookDTO dto = bookService.insert(insertDTO);
		return ResponseEntity.status(201).body(dto);
	}

	@PutMapping("/{id}")
	@Tag(name = "Atualiza um livro")
	public ResponseEntity<BookDTO> update(@PathVariable("id") Long id, @RequestBody @Valid BookInsertDTO insertDTO) {
		BookDTO dto = bookService.update(id, insertDTO);
		return ResponseEntity.status(200).body(dto);
	}

	@DeleteMapping("/{id}")
	@Tag(name = "Remove Livro por Id")
	public ResponseEntity<Void> remove(@PathVariable("id") Long id) {
		bookService.remove(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/author")
	@Tag(name = "Busca livros pelo autor")
	public ResponseEntity<Page<BookDTO>> findByAuthor(@PathParam(value = "name") String name, Pageable pageable) {
		Page<BookDTO> pageDto = bookService.findByAuthor(name, pageable);
		return ResponseEntity.status(200).body(pageDto);
	}

	@GetMapping("/publisher")
	@Tag(name = "Busca livros pela editora")
	public ResponseEntity<Page<BookDTO>> findByPublisher(@PathParam(value = "name") String name, Pageable pageable) {
		Page<BookDTO> pageDto = bookService.findByPublisher(name, pageable);
		return ResponseEntity.status(200).body(pageDto);
	}
}
