package dev.hygino.services;

import java.time.Instant;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.hygino.dto.PublisherDTO;
import dev.hygino.dto.PublisherInsertDTO;
import dev.hygino.entities.Publisher;
import dev.hygino.repositories.PublisherRepository;
import dev.hygino.services.exception.DatabaseException;
import dev.hygino.services.exception.ResourceNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@Service
public class PublisherService {

    private final ModelMapper mapper;
    private final PublisherRepository publisherRepository;

    public PublisherService(ModelMapper mapper, PublisherRepository publisherRepository) {
        this.mapper = mapper;
        this.publisherRepository = publisherRepository;
    }

    @Transactional
    public PublisherDTO insert(@Valid PublisherInsertDTO dto) {
        Publisher entity = mapper.map(dto, Publisher.class);
        entity.setCreateDate(Instant.now());
        entity = publisherRepository.save(entity);

        return mapper.map(entity, PublisherDTO.class);
    }

    @Transactional(readOnly = true)
    public PublisherDTO findById(Long id) {
        Optional<Publisher> obj = publisherRepository.findById(id);
        Publisher entity = obj.orElseThrow(() -> new ResourceNotFoundException("Entity not found"));

        return mapper.map(entity, PublisherDTO.class);
    }

    @Transactional(readOnly = true)
    public Page<PublisherDTO> findAll(Pageable pageable) {
        Page<Publisher> page = publisherRepository.findAll(pageable);

        return page.map(obj -> mapper.map(obj, PublisherDTO.class));
    }

    @Transactional
    public PublisherDTO update(Long id, @Valid PublisherInsertDTO dto) {
        try {
            Publisher entity = publisherRepository.getReferenceById(id);
            // instancia um objeto provisório
            entity.setName(dto.getName());
            entity.setUpdateDate(Instant.now());
            entity = publisherRepository.save(entity);
            return mapper.map(entity, PublisherDTO.class);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException("Id not found " + id);
        }
    }

    @Transactional
    public void remove(Long id) {
        try {
            publisherRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException("Id not found " + id);
        } catch (DataIntegrityViolationException e) {
            throw new DatabaseException("Integrity violation");
        }
    }
}
