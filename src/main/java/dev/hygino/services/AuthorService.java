package dev.hygino.services;

import java.time.Instant;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.hygino.dto.AuthorDTO;
import dev.hygino.dto.AuthorInsertDTO;
import dev.hygino.entities.Author;
import dev.hygino.repositories.AuthorRepository;
import dev.hygino.services.exception.DatabaseException;
import dev.hygino.services.exception.ResourceNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@Service
public class AuthorService {

	private final ModelMapper mapper;
	private final AuthorRepository authorRepository;

	public AuthorService(ModelMapper mapper, AuthorRepository authorRepository) {
		this.mapper = mapper;
		this.authorRepository = authorRepository;
	}

	@Transactional
	public AuthorDTO insert(@Valid AuthorInsertDTO dto) {
		Author entity = mapper.map(dto, Author.class);
		entity = authorRepository.save(entity);
		entity.setCreateDate(Instant.now());

		return mapper.map(entity, AuthorDTO.class);
	}

	@Transactional(readOnly = true)
	public Page<AuthorDTO> findAll(Pageable pageable) {
		Page<Author> page = authorRepository.findAll(pageable);
		return page.map(x -> mapper.map(x, AuthorDTO.class));
	}

	@Transactional(readOnly = true)
	public AuthorDTO findById(Long id) {
		Optional<Author> obj = authorRepository.findById(id);
		Author entity = obj.orElseThrow(() -> new ResourceNotFoundException("Entity not found"));

		return mapper.map(entity, AuthorDTO.class);
	}

	@Transactional
	public AuthorDTO update(Long id, @Valid AuthorInsertDTO dto) {

		try {
			Author entity = authorRepository.getReferenceById(id);
			// instancia um objeto provisório
			entity.setName(dto.getName());
			entity.setCountry(dto.getCountry());
			entity.setUpdateDate(Instant.now());
			entity = authorRepository.save(entity);
			return mapper.map(entity, AuthorDTO.class);
		} catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("Id not found " + id);
		}
	}

	@Transactional
	public void remove(Long id) {

		try {
			authorRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("Id not found " + id);
		} catch (DataIntegrityViolationException e) {
			throw new DatabaseException("Integrity violation");
		}
	}
}
