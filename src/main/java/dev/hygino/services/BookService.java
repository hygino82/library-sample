package dev.hygino.services;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.hygino.dto.BookDTO;
import dev.hygino.dto.BookInsertDTO;
import dev.hygino.entities.Author;
import dev.hygino.entities.Book;
import dev.hygino.entities.Publisher;
import dev.hygino.repositories.AuthorRepository;
import dev.hygino.repositories.BookRepository;
import dev.hygino.repositories.PublisherRepository;
import dev.hygino.services.exception.DatabaseException;
import dev.hygino.services.exception.ResourceNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Valid;

@Service
public class BookService {

	private static Logger logger = LoggerFactory.getLogger(BookService.class);

	private final ModelMapper mapper;
	private final BookRepository bookRepository;
	private final AuthorRepository authorRepository;
	private final PublisherRepository publisherRepository;

	public BookService(ModelMapper mapper, BookRepository bookRepository, AuthorRepository authorRepository,
			PublisherRepository publisherRepository) {
		this.mapper = mapper;
		this.bookRepository = bookRepository;
		this.authorRepository = authorRepository;
		this.publisherRepository = publisherRepository;
	}

	@Transactional(readOnly = true)
	public Page<BookDTO> findAll(Pageable pageable) {
		Page<Book> page = bookRepository.findAll(pageable);
		return page.map(obj -> mapper.map(obj, BookDTO.class));
	}

	@Transactional(readOnly = true)
	public BookDTO findById(Long id) {
		Book entity = bookRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Não foi possivel encontrar o livro com Id: " + id));

		return mapper.map(entity, BookDTO.class);
	}

	@Transactional
	public BookDTO insert(@Valid BookInsertDTO dto) {

		try {

			// Book entity = new Book();
			Author author = authorRepository.getReferenceById(dto.getAuthorId());

			if (author == null) {
				logger.error("Autor não encontrado");
				throw new ResourceNotFoundException("Não foi possivel encontrar o autor");
			}
			logger.info("Author encontrado: " + author);

			Publisher publisher = publisherRepository.getReferenceById(dto.getPublisherId());
			if (publisher == null) {
				logger.error("Editora não encontrada");
				throw new ResourceNotFoundException("Não foi possivel encontrar a editora");
			}
			logger.info("Editora encontrada: " + publisher);

			Book entity = new Book(dto, author, publisher);

			logger.info("Livro adicionado: " + entity);
			entity = bookRepository.save(entity);

			return mapper.map(entity, BookDTO.class);
		} catch (ConstraintViolationException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	@Transactional
	public BookDTO update(Long id, @Valid BookInsertDTO dto) {
		try {

			logger.error("Atualizando o livro com id: " + id);
			Book entity = bookRepository.getReferenceById(id);
			// Book entity = bookRepository.findById(id).orElseThrow(
			// () -> new ResourceNotFoundException("Não foi possivel encontrar o livro com
			// Id: " + id));

			Author author = authorRepository.getReferenceById(dto.getAuthorId());

			if (author == null) {
				logger.error("Autor não encontrado");
				throw new ResourceNotFoundException("Não foi possivel encontrar o autor");
			}
			logger.info("Author encontrado: " + author);

			Publisher publisher = publisherRepository.getReferenceById(dto.getPublisherId());
			if (publisher == null) {
				logger.error("Editora não encontrada");
				throw new ResourceNotFoundException("Não foi possivel encontrar a editora");
			}

			entity = propsToEntity(entity, dto, author, publisher);
			logger.info("Livro atualizado: " + entity);
			entity = bookRepository.save(entity);

			return mapper.map(entity, BookDTO.class);

		} catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("Id not found " + id);
		}
	}

	private Book propsToEntity(Book entity, BookInsertDTO dto, Author author, Publisher publisher) {

		entity.setEdition(dto.getEdition());
		entity.setPages(dto.getPages());
		entity.setEdition(dto.getEdition());
		entity.setIsbn(dto.getIsbn());
		entity.setYear(dto.getYear());
		entity.setPublisher(publisher);
		entity.setAuthor(author);

		return entity;
	}

	@Transactional
	public void remove(Long id) {
		try {
			bookRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("Id not found " + id);
		} catch (DataIntegrityViolationException e) {
			throw new DatabaseException("Integrity violation");
		}
	}

	@Transactional(readOnly = true)
	public Page<BookDTO> findByAuthor(String name, Pageable pageable) {
		logger.info("Buscando por autor que o nome contenha: " + name);

		Page<Book> page = bookRepository.findByAuthor(name, pageable);
		return page.map(obj -> mapper.map(obj, BookDTO.class));
	}

	@Transactional(readOnly = true)
    public Page<BookDTO> findByPublisher(String name, Pageable pageable) {
		logger.info("Buscando por editora que o nome contenha: " + name);

		Page<Book> page = bookRepository.findByPublisher(name, pageable);
		return page.map(obj -> mapper.map(obj, BookDTO.class));
    }
}
