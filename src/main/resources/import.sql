INSERT INTO tb_author(name, country, create_date) VALUES ('José de Alencar', 'Brazil', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('Paulo Coelho', 'Brazil', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('Monteiro Lobato', 'Brazil', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('Dalton Trevisan', 'Brazil', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('Clarice Lispector', 'Brazil', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('Lúcio Cardoso', 'Brazil', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('Stephen King', 'United States of America', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('Dan Brown', 'United States of America', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('James Rollins', 'United States of America', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('J. K. Rowling', 'England', NOW());
INSERT INTO tb_author(name, country, create_date) VALUES ('J.R.R. Tolkien', 'England', NOW());



INSERT INTO tb_publisher(name, create_date) VALUES('Arqueiro', NOW());
INSERT INTO tb_publisher(name, create_date) VALUES('Rocco', NOW());
INSERT INTO tb_publisher(name, create_date) VALUES('Civilização Brasileira', NOW());
INSERT INTO tb_publisher(name, create_date) VALUES('Suma', NOW());
INSERT INTO tb_publisher(name, create_date) VALUES('Companhia das Letras', NOW());
INSERT INTO tb_publisher(name, create_date) VALUES('Sextante', NOW());
INSERT INTO tb_publisher(name, create_date) VALUES('Melhoramentos', NOW());
INSERT INTO tb_publisher(name, create_date) VALUES('Principis', NOW());
INSERT INTO tb_publisher(name, create_date) VALUES('Martin Claret', NOW());

INSERT INTO tb_book(title, author_id, publisher_id, pages, edition, year, isbn, create_date) VALUES ('Iracema', 1, 7, 144, 1, 2012, '978-8506006948', NOW());
INSERT INTO tb_book(title, author_id, publisher_id, pages, edition, year, isbn, create_date) VALUES ('Lucíola', 1, 9, 152, 4, 2012, '978-8572325295', NOW());